import { useEffect, useState } from 'react';
import './App.css';
import SingleCard from './SingleCard/SingleCard';

const cardImages = [
  {"src":"https://www.ldoceonline.com/media/english/illustration/sword.jpg?version=1.2.58", "matched":false},//sword,
  {"src":"https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSSIZvtqpJZanFcBV_cDxLnXxtNku6h1GgB5w&usqp=CAU",  "matched":false},//gun,
  {"src":"https://economictimes.indiatimes.com/thumb/height-450,width-600,imgsize-728345,msid-81681351/brucelee.jpg?from=mdr",  "matched":false},//bruce lee
  {"src":"https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR_CjRCXayDWISYgKf4MV3I6urR20YdtvKO4Q&usqp=CAU", "matched":false}, //nan chaku
  {"src":"https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQgfcESnjmtec-XehkPilojm12N-vbjSTtlGQ&usqp=CAU", "matched":false}, //knife
  {"src":"https://cdni.autocarindia.com/ExtraImages/20211215012154_Toyota%20sports%20car.jpg", "matched":false}, //car
  {"src":"https://ic1.maxabout.us/autos/tw_india//D/2021/3/dhoom-3-bike-bmw-k1300r.jpg", "matched":false}, //bike
  {"src":"https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR_kOSBYD3jAGag1f4oKTLX5D0fjAzhL3ta5w&usqp=CAU", "matched":false} //military tank
]

function App() {
  const [cards, setCards] = useState([]);
  const [turns, setTurns] = useState(0);
  const [choiceOne, setChoiceOne] = useState(null);
  const [choiceTwo, setChoiceTwo] = useState(null);
  const [disable, setDisable] = useState(false);

  
  
  const shuffleCard = ()=>{
    const shuffledCards = [...cardImages, ...cardImages]
    .sort(()=> Math.random()-0.5)
    .map((card)=>({...card, id:Math.random()}))
    setChoiceOne(null)
    setChoiceTwo(null)
    setCards(shuffledCards)
    setTurns(0)
  }

  const handleCard=(card)=>{
    choiceOne ? setChoiceTwo(card):setChoiceOne(card);
  }
  
  useEffect(()=>{
   
    if(choiceOne && choiceTwo){
      setDisable(true)
      if(choiceOne.src === choiceTwo.src){

        setCards(cards.map(card => {
          if(card.src === choiceOne.src){
             return {...card, matched:true}
          }else{
            return card
          }
        }))

        resetTurns()
      }else{
        setTimeout(()=>{
          resetTurns()
        },1000)
      }
    }
  }, [choiceOne, choiceTwo])


  useEffect(()=>{
       shuffleCard()
  },[])


  const resetTurns = ()=>{
    setChoiceOne(null)
    setChoiceTwo(null)
    setTurns(prevTurns => prevTurns+1)
    setDisable(false)
  }

  return (
    <div className="App">
      <h1>Magic Match</h1>
      <button className="button" onClick={shuffleCard}>NEW GAME</button>

      <div className='card-grid'>
        {cards.map((card)=>(
           <SingleCard 
           key={card.id} 
           card = {card}
           handleCard = {handleCard}
           flipped = {card === choiceOne || card === choiceTwo || card.matched}
           disable = {disable}
           />
        ))}
      </div>
      <p>Turns: {turns}</p>

    </div>
  );
}

export default App;
