import './SingleCard.css'

const SingleCard = (props)=>{
  const {card, handleCard, flipped, disable} = props;

  const handleClick = () =>{
    if(!disable){
        handleCard(card)
    }
  }

  return (
    <div className='card' key={card.id}>
    <div className={flipped ? "flipped":""}>
      <img className='front' src={card.src} alt="card-front"/>
      <img className='back' src='https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQM2UfsYdMmCuLP7oRDpC0thc1vHyrBNJT7tw&usqp=CAU' onClick={handleClick} alt='card-back'/>
    </div>
  </div>
)}

export default SingleCard